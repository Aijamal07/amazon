package runner;

import org.apache.log4j.BasicConfigurator;
import org.openqa.selenium.JavascriptExecutor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

import java.io.FileInputStream;
import java.io.IOException;
import java.util.Properties;


public class TestRunner {
    private static Logger logger = LoggerFactory.getLogger(TestRunner.class);
    public static WebDriver driver;
    public static Properties prop;

    @BeforeClass
    public static void setUp() throws IOException {
      // Set up a simple configuration that logs on the console.
        BasicConfigurator.configure();
        FileInputStream config = new FileInputStream("src/test/resources/config.properties");
        prop = new Properties();
        prop.load(config);

        logger.info("Starting WebDriver");
        driver=new ChromeDriver();
        driver.get("https://www.homedepot.com");
        driver.manage().window().maximize();

    }

    @AfterClass
    public static void tearDown(){
        logger.info("Closing WebDriver");
        driver.quit();

    }

    public static void sleep(int seconds) {
        try {
            int time = seconds * 1000;
            logger.info("Sleep time is: " + time);
            Thread.sleep(time);
        } catch (InterruptedException e) {
            logger.error("Time sleep error!!!");
        }
    }
    public static void scroller (int xAxis, int yAxis){
        JavascriptExecutor js = (JavascriptExecutor) driver;
        String scroll = "window.scrollBy(" + xAxis + ", " + yAxis + ")";
        js.executeScript(scroll);
    }

}

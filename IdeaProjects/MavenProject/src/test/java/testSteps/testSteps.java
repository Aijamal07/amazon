package testSteps;

import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import runner.TestRunner;
import java.util.List;

public class testSteps extends TestRunner {
    private static Logger logger = LoggerFactory.getLogger(testSteps.class);

    @Test
    public void granger() throws InterruptedException {
        logger.info(prop.getProperty("url"));


        logger.info("Page wait 3 seconds");

        sleep(3);

        WebElement searchBox = driver.findElement(By.id("headerSearch"));
        logger.info("Clicking Search Box");
        searchBox.click();
        searchBox.clear();
        searchBox.sendKeys(prop.getProperty("item1"));
        sleep(3);
//        driver.findElement(By.xpath("//input[@name="searchQuery"]"));

//=====================================================Selectors============================================================
        WebElement searchButton = driver.findElement(By.id("headerSearchButton"));  // By id
        searchButton.click();
        sleep(3);

        scroller(0, 7000);

        List<WebElement>allProducts=driver.findElements(By.xpath("//div[@class='browse-search__pod col__true-12 col__6-12--xs col__4-12--sm col__3-12--md col__3-12--lg']"));
        logger.info("Total products: "+allProducts.size());
        logger.info(allProducts.get(0).getText());
        sleep(2);
        scroller(0, -7000);
        sleep(3);
        allProducts.get(0).click();

//        WebElement firstItem=driver.findElement(By.xpath("//div[@data-automation-id='podnode']"));
//        firstItem.click();

        //System.out.println();
//        WebElement searchButton = driver.findElement(By.cssSelector("button#headerSearchButton")); //By CSS
//        WebElement searchButton = driver.findElement(By.cssSelector("button[id='headerSearchButton']")); //By css
//        button[title='Submit Search'] css
        //      /html[1]/body[1]/div[1]/div[3]/div[1]/div[2]/div[2]/form[1]/div[1]/div[1]/input[1] - ABS
        //       //input[@id='headerSearch'] - rel XPAth

        ////div[@class="browse-search__pod col__true-12 col__6-12--xs col__4-12--sm col__3-12--md col__3-12--lg"]
//=================================================================================================================

       sleep(3);

    }
}

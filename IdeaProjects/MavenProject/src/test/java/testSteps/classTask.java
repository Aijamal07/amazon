package testSteps;

import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.Wait;
import runner.TestRunner;

import java.util.List;

public class classTask extends TestRunner {
    //    Open homedepot.com
//    Click Search box
//    Find Hammer click search
//    Wait for 3 seconds
//    Choose 3rd product
//    Click add to cart
//    Wait for 3 seconds
//    Click view cart
//    Wait 5 seconds
    @Test
    public void amazonTest() throws InterruptedException {

        WebElement searcher = driver.findElement(By.id("headerSearch"));
        searcher.click();
        searcher.clear();
        searcher.sendKeys("Hammer");
        WebElement searchButton=driver.findElement(By.xpath("//button[@id='headerSearchButton']"));
        searchButton.click();
        Thread.sleep(3000);

        JavascriptExecutor js=(JavascriptExecutor)driver;
        js.executeScript("window.scrollBy(0,7000)");

        List<WebElement> allProducts=driver.findElements(By.xpath("//div[@class='browse-search__pod col__true-12 col__6-12--xs col__4-12--sm col__3-12--md col__3-12--lg']"));
        System.out.println("Total products: "+allProducts.size());
        Thread.sleep(5000);
        js.executeScript("window.scrollBy(0,-6000)");
        Thread.sleep(5000);
        allProducts.get(3).click();
        Thread.sleep(5000);
        WebElement AddToButton=driver.findElement(By.xpath("//button[@class='bttn bttn--primary']"));
        AddToButton.click();
        Thread.sleep(6000);
//        WebElement viewCart=driver.findElement(By.xpath("//a[]"));
//        viewCart.click();
//        Thread.sleep(5000);
    }
}
